# An LTC5510 Based Active Mixer

## Features

- High linearity: OIP3 of 27 dBm at 1500 MHz, input P1dB 11 dBm.
- 50 ohm matched input for 30 to 3000 MHz.
- Up- or down-conversion.
- Input/LO range to 6000 MHz.
- 0 dBm LO drive level.
- Excellent LO to RF isolation: 65 dB at 1100 MHz. LO to IF isolation
  41 dB.
- Noise figure 11.6 dBm at 1500 MHz.

## Documentation

Full documentation for the mixer is available at
[RF Blocks](https://rfblocks.org/boards/LTC5510-Active-Mixer.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
